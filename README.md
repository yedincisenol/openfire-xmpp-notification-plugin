Openfire Push Notification Plugin
============

Push notification plugin for Openfire xmpp server

This plugin`s job is; on message sent, detect reciver account status;
if reciver account is offline, sent message meta to push notification web service adress (you can set on Openfire settings page)

This repository clone is https://github.com/meisterfuu/Openfire-GCM/ but change any code