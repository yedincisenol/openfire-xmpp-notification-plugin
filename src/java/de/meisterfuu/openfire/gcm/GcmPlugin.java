package de.meisterfuu.openfire.gcm;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.TimerTask;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.jivesoftware.openfire.PresenceManager;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.interceptor.InterceptorManager;
import org.jivesoftware.openfire.interceptor.PacketInterceptor;
import org.jivesoftware.openfire.interceptor.PacketRejectedException;
import org.jivesoftware.openfire.session.Session;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.UserNameManager;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.util.JiveGlobals;
import org.jivesoftware.util.PropertyEventListener;
import org.jivesoftware.util.TaskEngine;
import org.jivesoftware.openfire.muc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.JID;
import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;
import org.xmpp.packet.Presence;

import java.util.Collection;

import com.google.gson.Gson;

public class GcmPlugin implements Plugin, PacketInterceptor {

	private static final Logger Log = LoggerFactory.getLogger(GcmPlugin.class);
	private static final String URL = "plugin.gcmh.url";
	private static final String MODE = "plugin.gcmh.mode";
	private static final String DEBUG = "plugin.gcmh.debug";

	public static final String MODE_ALL = "1";
	public static final String MODE_OFFLINE = "2";
	public static final String MODE_NO_MOBILE = "3";
	public static final String MODE_EXCEPTION = "4";

	public static final String DEBUG_ON = "1";
	public static final String DEBUG_OFF = "2";
	

	public GcmPlugin() {
		interceptorManager = InterceptorManager.getInstance();
	}

	private InterceptorManager interceptorManager;
	private XMPPServer mServer;
	private PresenceManager mPresenceManager;
	private UserManager mUserManager;
	private Gson mGson;
	private MultiUserChatService mUserChatService;

	public void initializePlugin(PluginManager manager, File pluginDirectory) {
		Log.info("GCM Plugin started");
		
		initConf();
		mServer = XMPPServer.getInstance();
		mPresenceManager = mServer.getPresenceManager();
		mUserManager = mServer.getUserManager();
		mGson = new Gson();
		//mUserChatService = mServer.getMultiUserChatManager();
		
		interceptorManager.addInterceptor(this);
	}

	public void destroyPlugin() {
		Log.info("GCM Plugin destroyed");
		interceptorManager.removeInterceptor(this);
	}

	public void interceptPacket(Packet packet, Session session,
			boolean incoming, boolean processed) throws PacketRejectedException {

		if (processed) {
			return;
		}
		if (!incoming) {
			return;
		}

		if (packet instanceof Message) {
			Message msg = (Message) packet;
			process(msg);
		}else{
			if(packet instanceof Presence){
				
				Presence pre = (Presence) packet;
				if(pre.getType().toString() == "subscribe"){
					//Log.info(pre.toString());
					Message preMesage = new Message();
					preMesage.setFrom(pre.getFrom());
					preMesage.setTo(pre.getTo());
					//preMesage.setType(pre.getType());
					preMesage.setID("rosterRequest");
					//Log.info(preMesage.toString());
					process(preMesage);
				}
			}
		}

	}
	
	
	public static String getStackTrace(final Throwable throwable) {

	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}

	private void process(final Message msg) {

		if(mDebug)Log.info("GCM Plugin process() called"); 
		try {

			if(msg.toString().toLowerCase().contains("<composing xmlns=\"http://jabber.org/protocol/chatstates\"/>"))
				return ;
			if(msg.toString().toLowerCase().contains("<paused xmlns=\"http://jabber.org/protocol/chatstates\"/>"))
				return ;
			
			
			/*Log.info("mesaj"+msg.toString());
			
			/*if(msg.getBody().length() == 0)
				return ;
			*/
			
			JID toJID = msg.getTo().asBareJID();
			String y = UserNameManager.getUserName(toJID);
			
			String toUserName = toJID.toString().replace("@"+mServer.getServerInfo().getXMPPDomain(),""); 
			
			int position = toUserName.indexOf("@conference.panpachat.com");
			
			if(position > 0){
				
				if(msg.toString().toLowerCase().contains("<invite to=\"")){
					Log.info("Gruba gelen davet es gecildi");
					return ;
				}
				
		        JID recipient = msg.getTo();
		        String group = recipient.getNode();
		        Log.info(group);
				try{
					MultiUserChatManager mucManager = XMPPServer.getInstance().getMultiUserChatManager();
					MultiUserChatService mucService = mucManager.getMultiUserChatService(recipient);
					MUCRoom room = mucService.getChatRoom(recipient.getNode());
					Collection<JID> groupMembers = mucService.getChatRoom(recipient.getNode()).getMembers();

					
					
			        for (JID jid : groupMembers) {
			        	

			        	
			        	String newJid = jid.toString().replace("@"+mServer.getServerInfo().getXMPPDomain(),"");
			        	Log.info("grupUye:" + jid.toString());
			        	
			        	msg.setTo(jid);
			        	
						if (checkTarget(msg)) {
							
							msg.setID(toUserName);
							
							Log.info(newJid+" grup kullanicisina push cagrildi");
							/*
							TimerTask messageTask = new TimerTask() {
								@Override
								public void run() {
										sendExternalMsg(msg);
								}
							};
							TaskEngine.getInstance().schedule(messageTask, 20);
							*/ 
							sendExternalMsg(msg);
						} else {
							Log.info(newJid+" grup kullanicisina push cagrilmadi");
							if(mDebug)Log.info("GCM Plugin Check=false");
						}
			        }
					
					
				}catch(Exception e){
					

					String message = getStackTrace(e);
					Log.info(message);
					
				}
				

				
			}else{
				
				if (checkTarget(msg)) {
					Log.info("GCM Plugin "+toJID.toString()+" kullanicisina push cagrildi");
					/*
					TimerTask messageTask = new TimerTask() {
						@Override
						public void run() {
								sendExternalMsg(msg);
						}
					};
					TaskEngine.getInstance().schedule(messageTask, 20);
					*/
					
					sendExternalMsg(msg);
				} else {
					Log.info("GCM Plugin "+toJID.toString()+" kullanicisina push cagrilmadi");

				}
				
				
			}
				
			
		} catch (UserNotFoundException e) {
			Log.error("GCM Plugin (UserNotFoundException) Something went reeeaaaaally wrong");
			e.printStackTrace();
			// Something went reeeaaaaally wrong if you end up here!!
		}
	}

	private boolean checkTarget(Message msg) throws UserNotFoundException {
		/*
		if(msg.getBody() == null || msg.getBody().equals("")){
			return false;
		}*/
		
		JID toJID = msg.getTo().asBareJID();
		if(mDebug)Log.info("GCM Plugin check() called");

		if(!toJID.getDomain().contains(mServer.getServerInfo().getXMPPDomain())){
			return false;
		}
		
	
		
		if (mMode.equalsIgnoreCase(GcmPlugin.MODE_ALL)) {
			
			return true;
		} else if (mMode.equalsIgnoreCase(GcmPlugin.MODE_OFFLINE)) {
			
			String y = UserNameManager.getUserName(toJID);
			
			String toUserName = toJID.toString().replace("@"+mServer.getServerInfo().getXMPPDomain(),""); 

			User x = mUserManager.getUser(toUserName);
			

			try{
				Presence z = mPresenceManager.getPresence(x);

				String status = z.getStatus();

				if(z == null) return false;

				return false;
				
			} catch (Exception e) {
				e.printStackTrace();
				return true;
			}
		} else if (mMode.equalsIgnoreCase(GcmPlugin.MODE_NO_MOBILE)) {
			
		} else if (mMode.equalsIgnoreCase(GcmPlugin.MODE_EXCEPTION)) {

		}

		return true;
	}
	
	private void sendExternalMsg(Message msg) {
		if(mDebug)Log.info("GCM Plugin sendExternalMsg() called");

		if(mURL == null){
			Log.error("GCM Plugin: URL is null");
			return;
		}
		
		/*EventObject temp = new EventObject();
		temp.setBody(msg.getBody());
		temp.setTo(msg.getTo().toBareJID());
		temp.setFrom(msg.getFrom().toBareJID());
		*/
		//Log.info(msg.toString());


		try {
			if(mDebug){
				String x = Request
				.Post(mURL)
//				.bodyForm(Form.form().add("data", mGson.toJson(temp)).build())
				//.bodyString(mGson.toJson(temp), ContentType.APPLICATION_JSON)
				.bodyString(msg.toString(),ContentType.DEFAULT_TEXT)
				.execute()
				.returnContent().asString();
				Log.info("GCM Plugin sendMsg(): "+x);
			} else {
				Request
				.Post(mURL)
//				.bodyForm(Form.form().add("data", mGson.toJson(temp)).build())
				//.bodyString(mGson.toJson(temp), ContentType.APPLICATION_JSON)
				.bodyString(msg.toString(),ContentType.DEFAULT_TEXT)
				.execute();
			}
		} catch (ClientProtocolException e) {
			Log.error("GCM Plugin: ClientProtocolException");
			Log.error("GCM Plugin: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.error("GCM Plugin: IOException");
			Log.error("GCM Plugin: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e){
			Log.error("GCM Plugin: (Unknown)Exception");
			Log.error("GCM Plugin: " + e.getMessage());
			e.printStackTrace();
		}
	}


	String mURL, mMode;
	boolean mDebug = false;
	
	private void initConf() {
		mURL = this.getURL();
		mMode = this.getMode();
		if (this.getDebug()) {
			mDebug = true;
		} else {
			mDebug = false;
		}
	}

	public void setURL(String pURL) {
		JiveGlobals.setProperty(URL, pURL);
		initConf();
	}

	public String getURL() {
		return JiveGlobals.getProperty(URL, null);
	}

	public String getMode() {
		return JiveGlobals.getProperty(MODE, GcmPlugin.MODE_ALL);
	}

	public void setMode(String mode) {
		JiveGlobals.setProperty(MODE, mode);
		initConf();
	}

	public void setDebug(boolean mode) {
		if(mode){
			JiveGlobals.setProperty(DEBUG, GcmPlugin.DEBUG_ON);
		} else {
			JiveGlobals.setProperty(DEBUG, GcmPlugin.DEBUG_OFF);
		}
		initConf();
	}

	public boolean getDebug() {
		if(JiveGlobals.getProperty(DEBUG, GcmPlugin.DEBUG_OFF).equalsIgnoreCase(GcmPlugin.DEBUG_ON)){
			return true;
		} else {
			return false;
		}
	}

	

}
